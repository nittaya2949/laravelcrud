<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @section('css')
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">        
    @show
    <title>@yield('title')</title>
</head>
<body>
    <div class="container mt-5">
        {{-- <h1 class="main-red">green</h1>
        @include('layouts/navbar') --}}
        @yield('content')
        @yield('js')
        {{-- @include('footer') --}}
    </div>  


    <?php 
        // if(isset($id)){
        //     echo $id;
        // }else {
        //     echo "not found";
        // }
    ?>
    {{-- or --}}
    {{-- @if(isset($id))
        {{ $id }}   
    @else 
        <h1>not found</h1>
    @endif --}}
    {{-- or --}}
    {{-- {{ isset($id) ? $id : 'not found' }} --}}
    {{-- or --}}
    {{-- {{ $id ?? 'not found' }} --}}


    {{-- for  --}}
    <?php
        // for ($i=0; $i < 10 ; $i++) { 
        //     echo $i;
        // }
    ?>
    {{-- or  --}}
    {{-- @for ($i=0; $i < 10 ; $i++)
        <span>{{ $i }}</span>
    @endfor --}}
    {{-- or  --}}
    {{-- @foreach ([1,2,3,4] as $a)
        {{ $a }}
    @endforeach --}}
    {{-- or  --}}
    {{-- @forelse ($collection as $item)
        @if ()
            
        @endif
    @empty
        
    @endforelse --}}  
</body>
</html>