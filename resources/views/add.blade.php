@extends('layouts.master')
@section('title','Add')

@section('content')
    <form action="{{ url('people') }}" method="post">
        @csrf
        <div class="form-grop">
            <label for="">Firstname</label>
            <input type="text" class="form-control" name="fname">
        </div>
        <div class="form-grop">
            <label for="">Lastname</label>
            <input type="text" class="form-control" name="lname">
        </div>
        <div class="form-grop">
            <label for="">Age</label>
            <input type="text" class="form-control" name="age">
        </div> <br>
        <button type="submit" class="btn btn-success">Save</button>
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach  
            </ul>
        </div>
        @endif
    </form>
@endsection
