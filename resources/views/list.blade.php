@extends('layouts.master')
@section('title','List')
@section('css')
    @parent
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">    
@endsection
@section('content')
    @if (Session::has('message'))
        <div class="alert alert-success">        
            {{ Session::get('message') }}
        </div>    
    @endif
    <a href="{{ url('people/create') }}">
        <button type="submit" class="btn btn-success">Create</button>    
    </a> <br><br>
    <table class="table table-dark">
        <thead>
            <th>ID</th>
            <th>Fname</th>
            <th>Lname</th>
            <th>Age</th>
            <th>Created_at</th>
            <th>Updated_at</th>
            <th>Action</th>
        </thead>
        <tbody>
            @foreach ($people as $p)                           
            <tr>                
                <td>{{ $p->id }}</td>
                <td>{{ $p->fname }}</td>
                <td>{{ $p->lname }}</td>
                <td>{{ $p->age }}</td>
                <td>{{ $p->created_at }}</td>
                <td>{{ $p->updated_at }}</td>
                <td>
                    <div class="row">
                        <a href="{{ url('people/' . $p->id . '/edit') }}">
                            <button type="submit" class="btn btn-primary">Edit</button>                        
                        </a>
                        <form action="{{ url('people/' . $p->id) }}" method="post">
                            @csrf
                            @method('delete')
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>  
                    </div>                                 
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
@endsection

@section('js')
    <script>
    
    </script>
@endsection