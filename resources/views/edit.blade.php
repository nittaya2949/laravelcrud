@extends('layouts.master')
@section('title','Edit')

@section('content')
    <form action="{{ url('people',[$people->id]) }}" method="post">
        @csrf
        @method('put')
        <h1>Edit People</h1>
        <div class="form-grop">
            <label for="">Firstname</label>
            <input type="text" class="form-control" name="fname" value="{{ $people->fname }}">
        </div>
        <div class="form-grop">
            <label for="">Lastname</label>
            <input type="text" class="form-control" name="lname" value="{{ $people->lname }}">
        </div>
        <div class="form-grop">
            <label for="">Age</label>
            <input type="text" class="form-control" name="age" value="{{ $people->age }}">
        </div> <br>
        <button type="submit" class="btn btn-success">Save</button>
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach  
            </ul>
        </div>
        @endif
    </form>
@endsection
